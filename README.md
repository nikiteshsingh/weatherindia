# Weather app for Indian cities

This app aims to show weather prediction of Indian cities using open-weather api for next 7 days. Cities are pre selected and hardcoded in assets folder.

## Project features
- Pixel perfect UI
- Mobile friendly
- ChartJs integration
- open-weather api integration

## Project setup
```
npm install
```

### Compile and hot-reload for development
```
npm run serve
```

### Compile and minify for production
```
npm run build
```

### Todo:
- Move api secrets out of git repo
- Implement debouncing